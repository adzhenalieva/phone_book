import React, {Component} from 'react';
import Contact from "./component/Contact/Contact";

import './App.css';
import Modal from "./component/UI/Modal/Modal";

class App extends Component {
    state = {
        phones: [],
        changingContact: false,
        modalId: 0,
    };

    saveData = (data) => {
        localStorage.setItem('phones', JSON.stringify(data));
        let phones = JSON.parse(localStorage.getItem('phones'));
        this.setState({phones})
    };

    showModal = (event, id) => {
        this.setState({changingContact: true, modalId: id});
    };

    modalCancel = () => {
        this.setState({changingContact: false});
    };

    clickedLink = () => {
        alert('Link clicked');
    };

    changeValue = (event, id) => {
        let phones = [...this.state.phones];
        let contact = {...phones[id]};
        let name = event.target.name;
        contact[name] = event.target.value;
        phones[id] = contact;
        this.saveData(phones);
    };

    showMore = (id) => {
        let phones = [...this.state.phones];
        let contact = {...phones[id]};
        contact.class = "Show";
        contact.class2 = "Hide";
        phones[id] = contact;
        this.setState({phones});
    };

    hide = (id) => {
        let phones = [...this.state.phones];
        let contact = {...phones[id]};
        contact.class = "Hide";
        contact.class2 = "Show";
        phones[id] = contact;
        this.setState({phones});
    };

    componentDidMount() {
        if(JSON.parse(localStorage.getItem('phones')) !== null) {
          this.setState({phones: JSON.parse(localStorage.getItem('phones'))})
        } else{
            fetch('http://demo.sibers.com/users').then(response => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error('Unexpected error');
            }).then(contact => {

                const phones = contact.map(contact => {
                    return {
                        name: contact.name,
                        phone: contact.phone,
                        email: contact.email,
                        img: contact.avatar,
                        company: contact.company.name,
                        site: contact.website,
                        address: contact.address.streetC
                    };
                });


                for(let i = 0; i < phones.length;  i++){
                    phones[i].class = "Hide";
                }
                this.saveData(phones);
            }).catch(error => {
                console.log(error);
            });
        }
    }

    render() {
        return (
            <div className="App">
                <h3>My phone book</h3>
                {this.state.changingContact && this.state.phones !== null ?
                    <Modal show={this.state.changingContact}
                           close={this.modalCancel}
                           name={this.state.phones[this.state.modalId].name}
                           phone={this.state.phones[this.state.modalId].phone}
                           email={this.state.phones[this.state.modalId].email}
                           site={this.state.phones[this.state.modalId].site}
                           address={this.state.phones[this.state.modalId].address}
                           company={this.state.phones[this.state.modalId].company}
                           changeValue={(event) => this.changeValue(event, this.state.modalId)}

                    /> : null
                }
                {this.state.phones.map((phone, id) => (
                    <Contact
                        key={id}
                        name={phone.name}
                        phone={phone.phone}
                        email={phone.email}
                        img={phone.img}
                        site={phone.site}
                        address={phone.address}
                        company={phone.company}
                        onClick={(event) => this.showModal(event, id)}
                        clickLink={this.clickedLink}
                        showMore={() => this.showMore(id)}
                        hide={() => this.hide(id)}
                        class = {this.state.phones[id].class}
                        class2 = {this.state.phones[id].class2}
                    />
                ))
                }
            </div>
        );
    }
}

export default App;
