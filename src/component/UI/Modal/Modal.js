import React, {Fragment} from 'react';
import BackDrop from "../Backdrop/Backdrop";

import './Modal.css';


const Modal = props => {
    return (
        <Fragment>
            <BackDrop show={props.show}  onClick={props.close} />
            <div
                className="Modal"
                style={
                    {transform: props.show ? 'translateY(0)': 'translateY(-100vh)',
                        opacity: props.show ? '1': '0'}
                }>
                <input className="ModalInput" name="name" type="text" value={props.name} onChange={props.changeValue}/>
                <input className="ModalInput" name="phone" type="text" value={props.phone} onChange={props.changeValue}/>
                <input className="ModalInput" name="email" type="email" value={props.email} onChange={props.changeValue}/>
                <input className="ModalInput" name="address" type="text" value={props.address} onChange={props.changeValue}/>
                <input className="ModalInput" name="site" type="url" value={props.site} onChange={props.changeValue}/>
                <input className="ModalInput" name="company" type="text" value={props.company} onChange={props.changeValue}/>
                <button className="ModalButton" onClick={props.close}>Save changes</button>
            </div>
        </Fragment>
    );
};

export default Modal;
