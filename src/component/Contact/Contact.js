import React from 'react';
import "./Contact.css";

const Contact = props => {
        return (
            <div className="Contact">
                <img className="Image" src={props.img} alt="avatar"/>
                <p><strong>Name: </strong>{props.name}</p>
                <p><strong>Phone: </strong>{props.phone}</p>
                <button className="EditButton" onClick={props.onClick}>Edit</button>
                <button className={props.class2} onClick={props.showMore}>Show more</button>
                <div className={props.class}>
                    <p><strong>E-mail: </strong>{props.email}</p>
                    <p><strong>Address: </strong>{props.address}</p>
                    <p><strong>Web-site: </strong>
                        <button className="Site" onClick={props.clickLink}>{props.site} </button>
                    </p>
                    <p><strong>Company: </strong>Company: {props.company}</p>
                    <button className={props.class} onClick={props.hide}>Hide</button>
                </div>
            </div>
        );
};

export default Contact;